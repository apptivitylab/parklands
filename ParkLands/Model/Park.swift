//
//  Park.swift
//  ParkLands
//
//  Created by Jason Khong on 11/16/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import Foundation
import UIKit

class Park {
    var name: String
    var photo: UIImage?
    var location: String?
    var reviews: [Review] = []
    
    init?(name: String) {
        self.name = name
        
        if name.isEmpty {
            return nil
        }
    }
    
    func averageRating() -> Double {
        if reviews.count == 0 {
            return 0.0
        }

        var average: Double = 0.0
        for review in self.reviews {
            average += Double(review.rating)
        }
        return average / Double(reviews.count)
    }
}