//
//  ParkListViewController.swift
//  ParkLands
//
//  Created by Jason Khong on 11/12/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class ParkListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var parks: [Park]  = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRowAtIndexPath(selectedIndexPath, animated: animated)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func randomParkName() -> String {
        let dataset: [String] = ["Aya Sofya", "Colosseum", "Golden Gates Bridge", "Great Wall", "Pyramid of Giza", "Stonehenge", "Taj Mahal", "Victoria Falls"]
        let index: Int = Int(arc4random_uniform(UInt32(dataset.count)))
        return dataset[index]
    }
    
    @IBAction func addPark(sender: AnyObject) {
        let randomName = randomParkName()

        let park: Park = Park(name: randomName)!
        park.photo = UIImage(named: randomName)
        
        self.parks.append(park)
 
        self.tableView.reloadData()
    }
}

extension ParkListViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: .Subtitle, reuseIdentifier: "Cell")
        
        let park: Park = self.parks[indexPath.row]
        
        cell.textLabel?.text = park.name
        cell.imageView?.image = park.photo
        
        return cell
    }
}

extension ParkListViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailsVC: ParkDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ParkDetailViewController") as! ParkDetailViewController
        
        let park: Park = self.parks[indexPath.row]
        detailsVC.park = park
        
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}